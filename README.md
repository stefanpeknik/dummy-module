# Dummy module

This repo serves as an example of "how a module project can use Gitlab CI to create a new release".
The new release is created by packaging the project into a tarball and uploading it.
